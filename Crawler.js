const _ = require('lodash');
const axios = require('axios');
const cheerio = require('cheerio');
const puppeteer = require('puppeteer');
const path = require('path');
const fetch = require('node-fetch');

const seedUrls = require('./seed_urls.json');
const fs = require('fs');


const captureScreenshot = async (url) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  
  await page.setViewport({ width: 1920, height: 1080 });
  
  await page.goto(url, { waitUntil: 'networkidle0' });
  
  const screenshot = await page.screenshot({ fullPage: true });
  
  await browser.close();
  
  const base64Screenshot = screenshot.toString('base64');
  
  return base64Screenshot;
};

(async () => {

  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  let i=0;
  for (const seedUrl of seedUrls) {
    await page.goto(seedUrl);
    const currentUrl = page.url();
    
      const screenshot = await captureScreenshot(currentUrl);
    
      let fileName = `file_${i}.png`;
      const outputFilePath = path.join('./screenshots', fileName);
      i =i+1;
      fs.writeFileSync(outputFilePath, screenshot, { encoding: 'base64' });
    
    axios.get(currentUrl)
    .then(response => {
      const html = response.data;
      const $ = cheerio.load(html);
      const title = $('title').text();
      console.log(`Title: ${title}`);
      $('a').each((i,currentUrl ) => {
        const href = $(currentUrl).attr('href');
        const text = $(currentUrl).text();
        console.log(`${text} : ${href}`);
      });
    })
    .catch(error => {
      console.log(error);
    });

    const html = await page.content();
    const base64Image = await page.screenshot({ fullPage: true, encoding: 'base64' });
    
    console.log(`Seed URL: ${seedUrl}\nCurrent URL: ${currentUrl}\nHTML: ${html}\nBase64 Image: ${base64Image}`);

    const result = 
    {
      seed_url: seedUrl,
      current_url: currentUrl,
      html: html,
      base64_image: base64Image
    };
    const resultString = JSON.stringify(result);
    fs.appendFileSync('result.json', `${resultString}\n`);
  }

  await browser.close();
})();
